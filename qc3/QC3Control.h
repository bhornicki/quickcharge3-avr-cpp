#pragma once

#include <stdint.h>

namespace QC3Control
{
    #define DP_DDR       DDRB
    #define DP_PORT      PORTB
    #define DP_PIN       PB3
    #define DM_DDR       DDRB
    #define DM_PORT      PORTB
    #define DM_PIN       PB4
    #define CONFIG_CLASS_B      false
    #define CONFIG_CHECK_HANDSHAKE   false
    #define CONFIG_AUTO_ROUND   false
    
    enum class Millivolts:uint16_t
    {
        VOLTAGE_5  = 5000,
        VOLTAGE_9  = 9000,
        VOLTAGE_12 = 12000,
        VOLTAGE_20 = 20000,
    };
    
    /**
     *  @brief Starts the handshake with the QC source, specifying whether the source is "class B" compliant (up to 20V) or not.
     *  
     *  @details A handshake is needed to be able to set the voltage. 
     *  begin() may be left out, in which case the first method modifying the voltage will silently call begin(false).
     *
     *  begin() is **blocking code**. It waits for a fixed period counting from the start up of the Arduino to act because the handshake needs a minimum time. But this is most likely not a problem because if you need 9V or 12V in your application, there is no gain in proceeding when the voltage isn't there yet (because of the handshake). And by putting begin() (or a call to one of the setXXX() functions) at the end of setup() (or other initialization) you can even do stuff while waiting because it counts from Arduino startup.
     *  
     *  @see begin(), setMilliVoltage(unsigned int), set5V(), set9V(), set12V(), set20V()
     */
    void begin();

    bool set(const QC3Control::Millivolts mv);
    /**
     *  @brief (deprecated - use setMilliVoltage()) Sets the desired voltage of the QC source.
     *  
     *  @details Will set the passed voltage either using discrete (QC2) mode for 5V, 9V and 12V (for backwards compatibility with QC2Control API), or using continuous (QC3) mode for all other values. Please use setMilliVoltage() to avoid this behaviour.
     *  
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to setVoltage() will result in a call to begin() to do the handshake.
     *  @note Setting an unreachable voltage will result in the closest supported voltage being set.
     *  @note Calling this method on a QC2 charger will only work for 5V, 9V or 12V.
     *  
     *  @param [in] volt The desired voltage (between 3.6V and 12V).
     *  
     *  @see setMilliVoltage(unsigned int), set5V(), set9V(), set12V()
     */
    void setVoltage(float volt);

    /**
     *  @brief Sets the desired voltage of the QC3.0 source.
     *  
     *  @details Will always set the passed voltage using continuous (QC3) mode (even for 5V, 9V, 12V and 20V). 
     *  To force usage of discrete (QC2) mode, please use set5V(), set9V(), set12V() or set20V();
     *
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to setMilliVoltage() will result in a call to begin() to do the handshake.
     *  @note Setting an unreachable voltage will result in the closest supported voltage being set.
     *  @note Calling this method on a QC2 charger will not work.
     *  
     *  @see set5V(), set9V(), set12V()
     *
     *  @param [in] milliVolt The desired voltage in mV (between 3600mV and 12000mV).
     *  
     */
    void setMilliVoltage(unsigned int milliVolt);


    /**
     *  @brief (deprecated - use getMilliVoltage()) Return the voltage that the charger is supposed to currently provide.
     *  
     *  @details This will be a value between 3.6 and 12V.
     *  
     *  @note The library has no feedback if that voltage is actually provided. It just takes note of the operations requested to the charger and computes the voltage that the charger is supposed to output now.
     *  
     *  @see getMilliVoltage()
     *  
     *  @return The voltage that the charger is supposed to currently provide, in Volt
     */
    float getVoltage();

    /**
     *  @brief Return the voltage that the charger is supposed to currently provide.
     *  
     *  @details Same as getVoltage, but returns an integer value in milliVolts
     *  
     *  @note The library has no notion if the voltage is really set. It just tries to set it but if you just start it all of a QC3.0 source it should set the correct voltage.
     *  
     *  @see getVoltage()
     *  
     *  @return The voltage that the charger is supposed to currently provide, in milliVolt
     */
    unsigned int getMilliVoltage();


    /**
     *  @brief Increment the desired voltage of the QC3.0 source by 200mV.
     *  
     *  @details Will request an increment of the voltage by 200mV. Performing the increment request when the maximum value is already reached has no effect.
     *  
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to incrementVoltage() will result in a call to begin() to do the handshake, then the voltage will be incremented starting from 5V
     */
    bool incrementVoltage();


    /**
     *  @brief Decrement the desired voltage of the QC3.0 source by 200mV.
     *  
     *  @details Will request a decrement of the voltage by 200mV. Performing the decrement request when the minimum value is already reached has no effect.
     *  
     *  @note If no handshake has been done (via begin()) with the QC source, the first call to decrementVoltage() will result in a call to begin() to do the handshake, then the voltage will be decremented starting from 5V
     */
    bool decrementVoltage();





 
};

