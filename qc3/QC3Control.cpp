#define F_CPU 3600000UL

#include <avr/io.h>
#include <util/delay.h>
#include "QC3Control.h"

namespace QC3Control
{
	namespace
	{
		/*
		 * Local variables
		 */
#if (CONFIG_CHECK_HANDSHAKE == true)
		bool gHandshakeDone; //!< Is the handshake done?
#endif
		bool gContinuousMode; //!< Are we in continuous adjustment (QC3) mode?
		unsigned int gMilliVolts; //!< Voltage currently set (in mV). Using the word "now" instead of "current" to prevent confusion between "current" and "voltage" :-)
		/*
		 * QC specification voltage constants
		 */
		constexpr uint16_t QC3_MIN_VOLTAGE_MV = 3600;
		constexpr uint16_t QC3_CLASS_A_MAX_VOLTAGE_MV = 12000;
		constexpr uint16_t QC3_CLASS_B_MAX_VOLTAGE_MV = 20000;
		constexpr uint16_t QC3_MAX_VOLTAGE_MV = CONFIG_CLASS_B ? QC3_CLASS_B_MAX_VOLTAGE_MV : QC3_CLASS_A_MAX_VOLTAGE_MV;
		/*
		 * QC specification timings
		 * timing values for Portable Device are not available, indicative values for a HVDCP charger were taken from the uP7104 datasheet https://www.upi-semi.com/files/1889/1b8dae21-e91a-11e6-97d5-f1910565ec6d
		 */
		constexpr double QC_T_GLITCH_BC_DONE_MS = 1500;
		constexpr double QC_T_GLICH_V_CHANGE_MS = 60;
		constexpr double QC_T_ACTIVE_MS = 1;
		constexpr double QC_T_INACTIVE_MS = 1;

		/*
		 * Low level functions to obtain desired voltages    
		 */
		inline void dm0V()
		{
			DM_DDR |= (1 << DM_PIN); //out
			DM_PORT &= ~(1 << DM_PIN); //low
		}

		inline void dm600mV()
		{
			DM_DDR &= ~(1 << DM_PIN); //in
			DM_PORT &= ~(1 << DM_PIN); //HiZ
		}

		inline void dm3300mV()
		{
			DM_DDR |= (1 << DM_PIN); //out
			DM_PORT |= (1 << DM_PIN); //high

		}

		inline void dp600mV()
		{
			DP_DDR &= ~(1 << DP_PIN); //in
			DM_PORT &= ~(1 << DM_PIN); //HiZ
		}

		inline void dp3300mV()
		{
			DP_DDR |= (1 << DP_PIN); //out
			DP_PORT |= (1 << DP_PIN); //high
		}

		/*
		 * Other low level functions
		 */
		void switchToContinuousMode()
		{

			dp600mV();
			dm3300mV();

			_delay_ms(QC_T_GLICH_V_CHANGE_MS);
			//TODO: 
			gContinuousMode = true;
		}
	}

	void begin()
	{
		// The spec requires that D+ remains at 0.6V during _WaitTime.
		dp600mV(); // Setting D+ to 0.6V is done by default (Arduino pins are input on boot)
		// We're in "QC3 schema". There's no way to let D- "float", but setting it to 0.6V will prevent the D+/D- short from pulling D+/D- down
		dm600mV(); // Which is done by default

		//because the Arduino starts the right way we just wait till millis() passes
		//Has the advantage that if you call this last in setup, all other setup
		//stuff will be counted as "waiting" :)
		//    while(millis() < QC_T_GLITCH_BC_DONE_MS); 
		_delay_ms(QC_T_GLITCH_BC_DONE_MS);

		// After QC_T_GLITCH_BC_DONE_MS, the QC source removes the short between D+ and D- and pulls D- down. 
		// We need to stay in this state for at least 2 more milliseconds before we can start requesting voltages.
		dm0V(); // "Acknowledge" by simulating that we "follow" the internal pull down
		_delay_ms(2);

#if (CONFIG_CHECK_HANDSHAKE == true)
		gHandshakeDone = true;
#endif
		gContinuousMode = false;
	}

	bool set(const QC3Control::Millivolts mv)
	{
#if (CONFIG_CHECK_HANDSHAKE == true)
		if(!gHandshakeDone)
			begin();
#endif
		// Transition from continous to discrete values requires first going to 5V
		if(gContinuousMode && (mv != Millivolts::VOLTAGE_5))
			set(Millivolts::VOLTAGE_5);

		switch(mv)
		{
			case Millivolts::VOLTAGE_5:
				dp600mV();
				dm0V();
				break;
			case Millivolts::VOLTAGE_9:
				dp3300mV();
				dm600mV();
				break;
			case Millivolts::VOLTAGE_12:
				dp600mV();
				dm600mV();
				break;
			case Millivolts::VOLTAGE_20:
#if (CONFIG_CLASS_B)
				dp3300mV();
				dm3300mV();
#else
				return false;
#endif
				break;
				
			default:
				return false;
		};

		_delay_ms(QC_T_GLICH_V_CHANGE_MS);

		gMilliVolts = static_cast<uint16_t>(mv);
		gContinuousMode = false;
		return true;
	}

	bool incrementVoltage()
	{
#if (CONFIG_CHECK_HANDSHAKE == true)
		if(!gHandshakeDone)
			begin();
#endif

		if(gMilliVolts >= QC3_MAX_VOLTAGE_MV)
			return false;

		if(!gContinuousMode)
			switchToContinuousMode();
		// From http://www.onsemi.com/pub/Collateral/NCP4371-D.PDF :
		// "For the single request, an HVDCP recognizes a rising edge on D+ for an increment ..." 
		dp3300mV();
		_delay_ms(QC_T_ACTIVE_MS);
		dp600mV();
		_delay_ms(QC_T_INACTIVE_MS);


		gMilliVolts += 200;
		return true;
	}

	bool decrementVoltage()
	{
#if (CONFIG_CHECK_HANDSHAKE == true)
		if(!gHandshakeDone)
			begin();
#endif

		if(gMilliVolts <= QC3_MIN_VOLTAGE_MV)
			return false;

		if(!gContinuousMode)
			switchToContinuousMode();

		// From http://www.onsemi.com/pub/Collateral/NCP4371-D.PDF :
		// "... and falling edge on D− for a decrement" 
		dm600mV();
		_delay_ms(QC_T_ACTIVE_MS);
		dm3300mV();
		_delay_ms(QC_T_INACTIVE_MS);

		gMilliVolts -= 200;
		return true;
	}

	/* Returns the closest multiple of 200
	 * e.g. passing 4901 or 4950 or 4999 or 5000 or 5001 or 5050 or 5100 returns 5000
	 */
	unsigned int getClosestValidMilliVolt(unsigned int mV)
	{
		return 200 * ((mV + 99) / 200);
	}

	void setMilliVoltage(unsigned int milliVolt)
	{
#if (CONFIG_CHECK_HANDSHAKE == true)
		if(!gHandshakeDone)
			begin();
#endif

		if(milliVolt <= QC3_MIN_VOLTAGE_MV)
			milliVolt = QC3_MIN_VOLTAGE_MV;
		else if(milliVolt >= (QC3_MAX_VOLTAGE_MV))
			milliVolt = (QC3_MAX_VOLTAGE_MV);
#if (CONFIG_AUTO_ROUND == true)
		else
			milliVolt = getClosestValidMilliVolt(milliVolt);
#endif

		if(milliVolt == gMilliVolts)
			return;

		if(milliVolt > gMilliVolts)
			while(gMilliVolts < milliVolt)
				incrementVoltage();
		else
			while(gMilliVolts > milliVolt)
				decrementVoltage();
	}

	void setVoltage(float volt)
	{
#if (CONFIG_AUTO_ROUND == true)
		unsigned int milliVolt = getClosestValidMilliVolt(volt * 1000);
#else
		unsigned int milliVolt = static_cast<unsigned int>(volt * 1000);
#endif

		switch(milliVolt)
		{
			case 5000:
				set(Millivolts::VOLTAGE_5);
				break;
			case 9000:
				set(Millivolts::VOLTAGE_9);
				break;
			case 12000:
				set(Millivolts::VOLTAGE_12);
				break;
			default:
				setMilliVoltage(milliVolt);
		}
	}

	float getVoltage()
	{
		return gMilliVolts / 1000.0;
	}

	unsigned int getMilliVoltage()
	{
		return gMilliVolts;
	}


}