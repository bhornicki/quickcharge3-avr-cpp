#define F_CPU 3600000UL
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "qc3/QC3Control.h"
#include <inttypes.h>

constexpr float threshold_voltage = 5.3;

/*
 * TODO: Periodically test negotiated voltage and if it does not match expected one: re-establish QC3 connection and set voltage again
void init()
{
	//
    //TIMER
    //
	
	TCCR0B = (1<<CS02) | (0<<CS01) | (1<<CS00);	//prescaler 1024
	
	//
    //ADC
    //
	ADMUX = (1<<REFS0)					//1v1 internal Vref
			| (1<<ADLAR)				//left aligned
			| (0<<MUX1) | (1<<MUX0);	//ADC1 / PB2 as input
	
	
	DIDR0 = (1<<ADC0D) | (1<<ADC2D) | (1<<ADC3D)	//disable other ADC inputs
			| (1<<AIN1D) | (1<<AIN0D);	//disable comparator inputs
	
	ADCSRB = (1<<ADTS2) | (0<<ADTS1) | (0<<ADTS0);	//triggered by timer overflow
	
	ADCSRA = (1<<ADEN) | (1<<ADATE) | (1<<ADIE)		//enable, auto trigger, interrupts
			| (1<<ADPS2) | (1<<ADPS1) | (0<<ADPS0);	//prescaler 64
}
*/
ISR(INT_ADC_vect)
{
	constexpr uint8_t threshold = 5.3 /11 *256 /1.1;
}

int main() 
{

	QC3Control::begin();
	QC3Control::set(QC3Control::Millivolts::VOLTAGE_9);
	while(1);  //TODO: go to sleep instead
}

