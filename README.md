# QC3Control
C++ Attiny13A code for setting the voltage of a Quick Charge 3.0 compatible charger.

Project was intended to explore MPLAB X IDE functionalities and usefulness. Also it was a test of running C++ code on Attiny MCUs.
Git was not used, so unfortunately there are no documented evidences of me tinkering with the code. Project is being successfully used as an USB C adapter for 9V label printer.

**README WAS UPDATED IN HURRY AND MAY CONTAIN MISTAKES!!!**

All credits go to :

- [Hugatry's HackVlog](https://www.youtube.com/channel/UCHgeChD442K0ah-KxEg0PHw) because he came up with the idea and first code to control QC2. 
- Timo Engelgeer (Septillion) who made a nice wrapper for Quick Charge 2.0: [QC2Control](https://github.com/septillion-git/QC2Control). 
- Vincent Deconinck (vdeconinck) who updated Timo's project to Quick Charge 3.0: [QC2Control](https://github.com/vdeconinck/QC3Control) - this project is based on Vincent's code.

You should use Vincent's QC3Control code instead of mine!

## What does it do?
QC3Control makes it possible to set the voltage (even on the fly) of a Quick Charge 3.0 source like a mains charger or power bank by simulating the behaviour of a QC3 portable device. 

Of course, to take advantage of this library, the source needs to support the [Quick Charge 3.0](https://www.qualcomm.com/products/features/quick-charge) technology form [Qualcomm](https://www.qualcomm.com/). 
However, the library can also be used with a QC2.0 compatible charger if using QC3Control::set(QC3Control::Millivolts).

## Differences between QC2Control and QC3Control
Apart from that, the main protocol difference between QC2.0 and QC3.0 is the introduction of continuous voltages, plus the possibility to lower the voltage to 3.6V.

To reach QC3.0 continuous mode, D- must be set high, which can be achieved by using a new circuit which basically duplicates the D+ configuration to the D- side: a divider between VCC and GND plus a third resistor to the Arduino "DM" pin. In that case, outputting a HIGH level to the third resistor, a voltage of 3.3V or more can be set on USB D-. This is the recommended circuit as it does not require an additional Arduino pin.

## Class A vs Class B
QC3.0 class A is the most common, is mainly used for phone chargers and outputs up to 12V. It is supported by QC3Control and fully tested. Possible voltages are 5V (USB default), 9V and 12V, plus any value between 3.6V and 12V obtained by 200mV steps from 5V (or from the previous voltage reached using setMilliVoltage()). 

QC3.0 class B is more targeted at laptops and can output up to 20V. **WARNING:** this means that the Arduino will be **destroyed** if powered directly from the controlled output voltage, and the use of a separate USB output, power supply or voltage regulator is mandatory. That being said, class B is now supported by QC3Control although testing was limited, and it is enabled by calling begin(true) instead of begin(). Possible voltages are then 5V (USB default), 9V, 12V and 20V, plus any value between 3.6V and 20V obtained by 200mV steps from 5V (or from the previous voltage reached using setMilliVoltage()). 

### How to connect?
As indicated above, the library supports the 2 following configurations. In all cases, all you need is a few resistors.

Recommended "2-wire" circuit:

**WARNING: schematic is outdated** and assumes 5V operating voltage, R5 and R6 should be omitted. R2 and R4 should be replaced with 2k2 ones.

![QC3Control recommended circuit](extras/recommended_circuit.png)

The wire color for a normal USB-cable is:  
- V<sub>BUS</sub>: Red  
- Data+: Green  
- Data-: White  
- GND: Black

You're free to pick any pin on the AVR, just be sure to point to the right pins in QC3Control().

### Powering the AVR
Project assumes 3.3v as microcontroller operating voltage. In my case it was supplied by AMS1117-3.3 regulator.
